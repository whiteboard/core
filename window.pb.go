// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.14.0
// source: whiteboard/core/window.proto

package core

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// 窗口信息
type Window struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 窗口信息
	// @gotags: validate:"required"
	Info *WindowInfo `protobuf:"bytes,2,opt,name=info,proto3" json:"info,omitempty" validate:"required"`
	// 坐标
	Point *Point `protobuf:"bytes,3,opt,name=point,proto3" json:"point,omitempty"`
	// 最小尺寸
	Min *Size `protobuf:"bytes,5,opt,name=min,proto3" json:"min,omitempty"`
	// 最佳尺寸
	Size *Size `protobuf:"bytes,6,opt,name=size,proto3" json:"size,omitempty"`
	// 最大尺寸
	Max *Size `protobuf:"bytes,7,opt,name=max,proto3" json:"max,omitempty"`
	// 外边距
	Margin *Gauge `protobuf:"bytes,10,opt,name=margin,proto3" json:"margin,omitempty"`
	// 内边距
	Padding *Gauge `protobuf:"bytes,11,opt,name=padding,proto3" json:"padding,omitempty"`
	// 滚动条
	Scrollbar *Scrollbar `protobuf:"bytes,13,opt,name=scrollbar,proto3" json:"scrollbar,omitempty"`
	// 状态
	Status WindowStatus `protobuf:"varint,14,opt,name=status,proto3,enum=whiteboard.core.WindowStatus" json:"status,omitempty"`
	// Z轴
	ZIndex int32 `protobuf:"varint,15,opt,name=z_index,json=zIndex,proto3" json:"z_index,omitempty"`
}

func (x *Window) Reset() {
	*x = Window{}
	if protoimpl.UnsafeEnabled {
		mi := &file_whiteboard_core_window_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Window) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Window) ProtoMessage() {}

func (x *Window) ProtoReflect() protoreflect.Message {
	mi := &file_whiteboard_core_window_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Window.ProtoReflect.Descriptor instead.
func (*Window) Descriptor() ([]byte, []int) {
	return file_whiteboard_core_window_proto_rawDescGZIP(), []int{0}
}

func (x *Window) GetInfo() *WindowInfo {
	if x != nil {
		return x.Info
	}
	return nil
}

func (x *Window) GetPoint() *Point {
	if x != nil {
		return x.Point
	}
	return nil
}

func (x *Window) GetMin() *Size {
	if x != nil {
		return x.Min
	}
	return nil
}

func (x *Window) GetSize() *Size {
	if x != nil {
		return x.Size
	}
	return nil
}

func (x *Window) GetMax() *Size {
	if x != nil {
		return x.Max
	}
	return nil
}

func (x *Window) GetMargin() *Gauge {
	if x != nil {
		return x.Margin
	}
	return nil
}

func (x *Window) GetPadding() *Gauge {
	if x != nil {
		return x.Padding
	}
	return nil
}

func (x *Window) GetScrollbar() *Scrollbar {
	if x != nil {
		return x.Scrollbar
	}
	return nil
}

func (x *Window) GetStatus() WindowStatus {
	if x != nil {
		return x.Status
	}
	return WindowStatus_WINDOW_STATUS_UNSPECIFIED
}

func (x *Window) GetZIndex() int32 {
	if x != nil {
		return x.ZIndex
	}
	return 0
}

var File_whiteboard_core_window_proto protoreflect.FileDescriptor

var file_whiteboard_core_window_proto_rawDesc = []byte{
	0x0a, 0x1c, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f, 0x63, 0x6f, 0x72,
	0x65, 0x2f, 0x77, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0f,
	0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2e, 0x63, 0x6f, 0x72, 0x65, 0x1a,
	0x21, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f, 0x63, 0x6f, 0x72, 0x65,
	0x2f, 0x77, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x1a, 0x23, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f, 0x63,
	0x6f, 0x72, 0x65, 0x2f, 0x77, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x5f, 0x73, 0x74, 0x61, 0x74, 0x75,
	0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1b, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f,
	0x61, 0x72, 0x64, 0x2f, 0x63, 0x6f, 0x72, 0x65, 0x2f, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1a, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64,
	0x2f, 0x63, 0x6f, 0x72, 0x65, 0x2f, 0x73, 0x69, 0x7a, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x1a, 0x1b, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f, 0x63, 0x6f, 0x72,
	0x65, 0x2f, 0x67, 0x61, 0x75, 0x67, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1f, 0x77,
	0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f, 0x63, 0x6f, 0x72, 0x65, 0x2f, 0x73,
	0x63, 0x72, 0x6f, 0x6c, 0x6c, 0x62, 0x61, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xd0,
	0x03, 0x0a, 0x06, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x12, 0x2f, 0x0a, 0x04, 0x69, 0x6e, 0x66,
	0x6f, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1b, 0x2e, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62,
	0x6f, 0x61, 0x72, 0x64, 0x2e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77,
	0x49, 0x6e, 0x66, 0x6f, 0x52, 0x04, 0x69, 0x6e, 0x66, 0x6f, 0x12, 0x2c, 0x0a, 0x05, 0x70, 0x6f,
	0x69, 0x6e, 0x74, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x16, 0x2e, 0x77, 0x68, 0x69, 0x74,
	0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x50, 0x6f, 0x69, 0x6e,
	0x74, 0x52, 0x05, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x12, 0x27, 0x0a, 0x03, 0x6d, 0x69, 0x6e, 0x18,
	0x05, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x15, 0x2e, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61,
	0x72, 0x64, 0x2e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x53, 0x69, 0x7a, 0x65, 0x52, 0x03, 0x6d, 0x69,
	0x6e, 0x12, 0x29, 0x0a, 0x04, 0x73, 0x69, 0x7a, 0x65, 0x18, 0x06, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x15, 0x2e, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2e, 0x63, 0x6f, 0x72,
	0x65, 0x2e, 0x53, 0x69, 0x7a, 0x65, 0x52, 0x04, 0x73, 0x69, 0x7a, 0x65, 0x12, 0x27, 0x0a, 0x03,
	0x6d, 0x61, 0x78, 0x18, 0x07, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x15, 0x2e, 0x77, 0x68, 0x69, 0x74,
	0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x53, 0x69, 0x7a, 0x65,
	0x52, 0x03, 0x6d, 0x61, 0x78, 0x12, 0x2e, 0x0a, 0x06, 0x6d, 0x61, 0x72, 0x67, 0x69, 0x6e, 0x18,
	0x0a, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x16, 0x2e, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61,
	0x72, 0x64, 0x2e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x47, 0x61, 0x75, 0x67, 0x65, 0x52, 0x06, 0x6d,
	0x61, 0x72, 0x67, 0x69, 0x6e, 0x12, 0x30, 0x0a, 0x07, 0x70, 0x61, 0x64, 0x64, 0x69, 0x6e, 0x67,
	0x18, 0x0b, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x16, 0x2e, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f,
	0x61, 0x72, 0x64, 0x2e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x47, 0x61, 0x75, 0x67, 0x65, 0x52, 0x07,
	0x70, 0x61, 0x64, 0x64, 0x69, 0x6e, 0x67, 0x12, 0x38, 0x0a, 0x09, 0x73, 0x63, 0x72, 0x6f, 0x6c,
	0x6c, 0x62, 0x61, 0x72, 0x18, 0x0d, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x77, 0x68, 0x69,
	0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x53, 0x63, 0x72,
	0x6f, 0x6c, 0x6c, 0x62, 0x61, 0x72, 0x52, 0x09, 0x73, 0x63, 0x72, 0x6f, 0x6c, 0x6c, 0x62, 0x61,
	0x72, 0x12, 0x35, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x0e, 0x20, 0x01, 0x28,
	0x0e, 0x32, 0x1d, 0x2e, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2e, 0x63,
	0x6f, 0x72, 0x65, 0x2e, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73,
	0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x17, 0x0a, 0x07, 0x7a, 0x5f, 0x69, 0x6e,
	0x64, 0x65, 0x78, 0x18, 0x0f, 0x20, 0x01, 0x28, 0x05, 0x52, 0x06, 0x7a, 0x49, 0x6e, 0x64, 0x65,
	0x78, 0x42, 0x32, 0x0a, 0x13, 0x63, 0x6f, 0x6d, 0x2e, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f,
	0x61, 0x72, 0x64, 0x2e, 0x63, 0x6f, 0x72, 0x65, 0x50, 0x01, 0x5a, 0x19, 0x67, 0x69, 0x74, 0x65,
	0x61, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64,
	0x2f, 0x63, 0x6f, 0x72, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_whiteboard_core_window_proto_rawDescOnce sync.Once
	file_whiteboard_core_window_proto_rawDescData = file_whiteboard_core_window_proto_rawDesc
)

func file_whiteboard_core_window_proto_rawDescGZIP() []byte {
	file_whiteboard_core_window_proto_rawDescOnce.Do(func() {
		file_whiteboard_core_window_proto_rawDescData = protoimpl.X.CompressGZIP(file_whiteboard_core_window_proto_rawDescData)
	})
	return file_whiteboard_core_window_proto_rawDescData
}

var file_whiteboard_core_window_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_whiteboard_core_window_proto_goTypes = []interface{}{
	(*Window)(nil),     // 0: whiteboard.core.Window
	(*WindowInfo)(nil), // 1: whiteboard.core.WindowInfo
	(*Point)(nil),      // 2: whiteboard.core.Point
	(*Size)(nil),       // 3: whiteboard.core.Size
	(*Gauge)(nil),      // 4: whiteboard.core.Gauge
	(*Scrollbar)(nil),  // 5: whiteboard.core.Scrollbar
	(WindowStatus)(0),  // 6: whiteboard.core.WindowStatus
}
var file_whiteboard_core_window_proto_depIdxs = []int32{
	1, // 0: whiteboard.core.Window.info:type_name -> whiteboard.core.WindowInfo
	2, // 1: whiteboard.core.Window.point:type_name -> whiteboard.core.Point
	3, // 2: whiteboard.core.Window.min:type_name -> whiteboard.core.Size
	3, // 3: whiteboard.core.Window.size:type_name -> whiteboard.core.Size
	3, // 4: whiteboard.core.Window.max:type_name -> whiteboard.core.Size
	4, // 5: whiteboard.core.Window.margin:type_name -> whiteboard.core.Gauge
	4, // 6: whiteboard.core.Window.padding:type_name -> whiteboard.core.Gauge
	5, // 7: whiteboard.core.Window.scrollbar:type_name -> whiteboard.core.Scrollbar
	6, // 8: whiteboard.core.Window.status:type_name -> whiteboard.core.WindowStatus
	9, // [9:9] is the sub-list for method output_type
	9, // [9:9] is the sub-list for method input_type
	9, // [9:9] is the sub-list for extension type_name
	9, // [9:9] is the sub-list for extension extendee
	0, // [0:9] is the sub-list for field type_name
}

func init() { file_whiteboard_core_window_proto_init() }
func file_whiteboard_core_window_proto_init() {
	if File_whiteboard_core_window_proto != nil {
		return
	}
	file_whiteboard_core_window_info_proto_init()
	file_whiteboard_core_window_status_proto_init()
	file_whiteboard_core_point_proto_init()
	file_whiteboard_core_size_proto_init()
	file_whiteboard_core_gauge_proto_init()
	file_whiteboard_core_scrollbar_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_whiteboard_core_window_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Window); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_whiteboard_core_window_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_whiteboard_core_window_proto_goTypes,
		DependencyIndexes: file_whiteboard_core_window_proto_depIdxs,
		MessageInfos:      file_whiteboard_core_window_proto_msgTypes,
	}.Build()
	File_whiteboard_core_window_proto = out.File
	file_whiteboard_core_window_proto_rawDesc = nil
	file_whiteboard_core_window_proto_goTypes = nil
	file_whiteboard_core_window_proto_depIdxs = nil
}
