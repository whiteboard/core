package core

const (
	// AllBoardTopic 所有的面板
	AllBoardTopic string = `+/b/+`
	// AllShareBoardTopic 所有共享订阅面板
	AllShareBoardTopic = `$queue/` + AllBoardTopic
)
