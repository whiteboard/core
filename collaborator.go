//go:build !deprecated
// +build !deprecated

package core

import (
	`github.com/storezhang/gox`
	`github.com/storezhang/gox/field`
)

func (c *Collaborator) ToUser() *User {
	return &User{
		Id:       c.Id,
		Nickname: c.Nickname,
		Type:     c.Type,
	}
}

func (c *Collaborator) Hosted() bool {
	return UserType(1) == c.Type || UserType(2) == c.Type
}

func (c *Collaborator) Fields() (fields gox.Fields) {
	fields = make([]gox.Field, 0, 4)
	fields = append(fields, field.String(`collaborator.id`, c.Id))
	fields = append(fields, field.String(`collaborator.nickname`, c.Nickname))
	fields = append(fields, field.String("collaborator.type", c.Type.Display()))
	if `` != c.Phone {
		fields = append(fields, field.String(`collaborator.phone`, c.Phone))
	}

	return
}
