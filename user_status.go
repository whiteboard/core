//go:build !deprecated
// +build !deprecated

package core

import (
	`github.com/storezhang/gox`
	`github.com/storezhang/gox/field`
)

func (us UserStatus) Fields() gox.Fields {
	return []gox.Field{
		field.String(`user.status`, us.Display()),
	}
}

func (us UserStatus) Display() (display string) {
	switch int(us) {
	case 10:
		display = "上线"
	case 11:
		display = "下线"
	default:
		display = "日了狗了，不要乱用状态"
	}

	return
}
