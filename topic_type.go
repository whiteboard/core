package core

const (
	// TopicTypeUnknown 未知
	TopicTypeUnknown TopicType = 0
	// TopicTypeUser 用户
	TopicTypeUser TopicType = 10
	// TopicTypeBoard 面板
	TopicTypeBoard TopicType = 20
	// TopicTypeGroup 组
	TopicTypeGroup TopicType = 30
)

// TopicType 主题类型
type TopicType uint8
