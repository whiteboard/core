//go:build !deprecated
// +build !deprecated

package core

import (
	`github.com/storezhang/gox`
	`github.com/storezhang/gox/field`
)

func (u *User) Hosted() bool {
	return UserType(1) == u.Type || UserType(2) == u.Type || UserType(5) == u.Type
}

func (u *User) Fields() (fields gox.Fields) {
	fields = make([]gox.Field, 0, 4)
	fields = append(fields, field.String(`user.id`, u.Id))
	fields = append(fields, field.String(`user.nickname`, u.Nickname))
	fields = append(fields, field.String("user.type", u.Type.Display()))
	if `` != u.Phone {
		fields = append(fields, field.String(`user.phone`, u.Phone))
	}

	return
}
