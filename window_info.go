//go:build !deprecated
// +build !deprecated

package core

import (
	`github.com/storezhang/gox`
	`github.com/storezhang/gox/field`
)

func (wi *WindowInfo) Fields() (fields gox.Fields) {
	fields = make([]gox.Field, 0, 2)
	fields = append(fields, field.String(`window.id`, wi.Id))
	if `` != wi.Title {
		fields = append(fields, field.String(`window.title`, wi.Title))
	}

	return
}
