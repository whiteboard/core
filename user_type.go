package core

func (ut UserType) Display() (display string) {
	switch int(ut) {
	case 0:
		display = `系统`
	case 1:
		display = `讲师`
	case 2:
		display = `助教`
	case 3:
		display = `学生`
	case 4:
		display = `游客`
	case 5:
		display = `监督员`
	default:
		display = `日了狗了，不要乱用类型`
	}

	return
}
