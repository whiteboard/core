package core

const (
	// TopicLabelUnknown 未知
	TopicLabelUnknown TopicLabel = ""
	// TopicLabelUser 用户
	TopicLabelUser TopicLabel = "u"
	// TopicLabelBoard 面板
	TopicLabelBoard TopicLabel = "b"
)

// TopicLabel 主题标签
type TopicLabel string
